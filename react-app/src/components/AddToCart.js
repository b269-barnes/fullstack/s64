import { useState } from "react";
import { Button } from "react-bootstrap";

export default function AddToCart({ productId, productName, price, stock }) {
  const [quantity, setQuantity] = useState(1);

  const addToCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId,
        quantity,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Swal.fire({
            title: "Succesfully ordered!",
            icon: "success",
            text: "You have successfully ordered this product.",
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  return (
    <div className="d-flex align-items-center justify-content-center">
      <Button
        variant="primary"
        onClick={addToCart}
        disabled={quantity > stock || stock <= 0}
      >
        {quantity > stock || stock <= 0 ? "Out of Stock" : "Add to Cart"}
      </Button>
      <div className="ml-3">
        <input
          type="number"
          min="1"
          max={stock}
          value={quantity}
          onChange={(e) => setQuantity(e.target.value)}
          style={{ width: "60px" }}
        />
      </div>
    </div>
  );
}
