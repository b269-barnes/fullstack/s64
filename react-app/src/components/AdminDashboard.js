import { useContext, useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function AdminDashboard() {
  const { user } = useContext(UserContext);
  const [allProducts, setAllProducts] = useState([]);



  const fetchData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setAllProducts(
          data.map((product) => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>${product.price}</td>
              <td>{product.stock}</td>
              <td>{product.isActive ? "Active" : "Inactive"}</td>
             <td>
               {product.isActive ? (
                 <>
                   <Button
                     as={Link}
                     to={`/updateProduct/${product._id}`}
                     variant="secondary"
                     size="sm"
                     className="mr-2"
                   >
                     Update
                   </Button>
                   <Button
                     variant="danger"
                     size="sm"
                     onClick={() => archiveProduct(product._id, product.name)}
                     className="ml-2"
                   >
                     Disable
                   </Button>
                 </>
               ) : (
                 <Button
                   variant="success"
                   size="sm"
                   onClick={() => unarchiveProduct(product._id, product.name)}
                 >
                   Enable
                 </Button>
               )}
             </td>
            </tr>
          ))
        );
      });
  };

  const archiveProduct = (productId, productName) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Product Disabled!",
            icon: "success",
            text: `${productName} is now disabled.`,
          });
          fetchData();
        } else {
          Swal.fire({
            title: "Disable Product Failed!",
            icon: "error",
            text: `Something went wrong. Please try again later!`,
          });
        }
      });
  };

  const unarchiveProduct = (productId, productName) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Product Enabled!",
            icon: "success",
            text: `${productName} is now enabled.`,
          });
            } else {
              Swal.fire({
                title: "Enable Product Failed!",
                icon: "error",
                text: `Something went wrong. Please try again later!`,
              });
            }
          });
};

useEffect(() => {
fetchData() 
}, []);

if (user && user.isAdmin) {
return (
<>
<div className="mt-5 mb-3 text-center">
        <h1>Admin Dashboard</h1>
        {/*A button to add a new course*/}
        <Button as={Link} to="/addProduct" variant="primary" size="md" className="mx-2">Add Product</Button>
        <Button as={Link} to="/showOrders" variant="success" size="md" className="mx-2">Show Order History</Button>
      </div>

<Table striped bordered hover className="table-light">
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Price</th>
      <th>Stocks</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>{allProducts}</tbody>
</Table>
</>
);
} else {
return <Navigate to="/" />;
}
}