import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "ClickShop",
		content: "Shop with ease, click with pleasure.",
		destination: "/home",
		label: "Click now!"
	}


	return (
		<>
		<a> <Banner data={data} /> </a>
    	<Highlights />
    	
		</>
	)
}


