import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button, Card } from 'react-bootstrap';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  // to store values of the input fields
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  // to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      fullName !== '' &&
      mobileNo.length === 11 &&
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [fullName, email, mobileNo, password1, password2]);

  // function to simulate user registration
  function registerUser(e) {
    // Prevents page from reloading
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: 'Duplicate Email Found',
            icon: 'error',
            text: 'Kindly provide another email to complete registration.',
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              fullName: fullName,
              email: email,
              mobileNo: mobileNo,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                // Clear input fields
                setFullName('');
                setEmail('');
                setMobileNo('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                  title: 'Registration Successful',
                  icon: 'success',
                  text: 'Welcome to Zuitt!',
                });

                navigate('/login');
              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please, try again.',
                });
              }
            });
        }
      });
  }

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <Card>
 <Card.Header style={{ fontWeight: 'bold' }} className= "text-center">Register</Card.Header>
      <Card.Body>
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group className="mb-3 " controlId="fullName">
            <Form.Label>Full Name</Form.Label>
            <Form.Control
             
              type="text"
              value={fullName}
              onChange={(e) => {
                setFullName(e.target.value);
              }}
              placeholder="Enter your Full Name"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email
            </Form.Label>
            <Form.Control
         
            type="email"
            value={email}
            onChange={(e) => {
            setEmail(e.target.value);
            }}
            placeholder="Enter your email address"
            />
            </Form.Group>
                  <Form.Group className="mb-3" controlId="mobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control
                
                      type="text"
                      value={mobileNo}
                      onChange={(e) => {
                        setMobileNo(e.target.value);
                      }}
                      placeholder="Enter your Mobile Number"
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                  
                      type="password"
                      value={password1}
                      onChange={(e) => {
                        setPassword1(e.target.value);
                      }}
                      placeholder="Enter your Password"
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                  
                      type="password"
                      value={password2}
                      onChange={(e) => {
                        setPassword2(e.target.value);
                      }}
                      placeholder="Confirm your Password"
                    />
                  </Form.Group>

                  <Button variant="primary" type="submit" disabled={!isActive}>
                    Register
                  </Button>
                </Form>
              </Card.Body>
            </Card>
            );
            }