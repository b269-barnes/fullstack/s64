	import { useState, useEffect } from "react";
	import { Button, Table } from "react-bootstrap";
	import Swal from "sweetalert2";

	function Cart() {
	  const [cart, setCart] = useState([]);

	  useEffect(() => {
	    // Fetch cart items from the server
	    fetch(`${process.env.REACT_APP_API_URL}/cart`, {
	      headers: {
	        Authorization: `Bearer ${localStorage.getItem("token")}`,
	      },
	    })
	      .then((res) => res.json())
	      .then((data) => {
	        setCart(data);
	      })
	      .catch((error) => {
	        console.error("Error fetching cart items:", error);
	      });
	  }, []);

	  const removeFromCart = (productId) => {
	    // Remove item from the cart
	    fetch(`${process.env.REACT_APP_API_URL}/cart/${productId}`, {
	      method: "DELETE",
	      headers: {
	        Authorization: `Bearer ${localStorage.getItem("token")}`,
	      },
	    })
	      .then((res) => res.json())
	      .then((data) => {
	        if (data === true) {
	          setCart((prevItems) =>
	            prevItems.filter((item) => item.productId !== productId)
	          );
	          Swal.fire({
	            title: "Removed from cart",
	            icon: "success",
	            text: "The item has been removed from your cart.",
	          });
	        } else {
	          Swal.fire({
	            title: "Something went wrong",
	            icon: "error",
	            text: "Please try again.",
	          });
	        }
	      })
	      .catch((error) => {
	        console.error("Error removing item from cart:", error);
	      });
	  };

	  const getTotalPrice = () => {
	    return cart.reduce(
	      (total, item) => total + item.price * item.quantity,
	      0
	    );
	  };

	  const handleCheckout = () => {
	    // Place order and clear cart
	    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
	      method: "POST",
	      headers: {
	        "Content-Type": "application/json",
	        Authorization: `Bearer ${localStorage.getItem("token")}`,
	      },
	      body: JSON.stringify({
	        items: cart.map((item) => ({
	          productId: item.productId,
	          quantity: item.quantity,
	        })),
	      }),
	    })
	      .then((res) => res.json())
	      .then((data) => {
	        if (data === true) {
	          setCart([]);
	          Swal.fire({
	            title: "Order placed",
	            icon: "success",
	            text: "Your order has been placed.",
	          });
	        } else {
	          Swal.fire({
	            title: "Something went wrong",
	            icon: "error",
	            text: "Please try again.",
	          });
	        }
	      })
	      .catch((error) => {
	        console.error("Error placing order:", error);
	      });
	  };

	  return (
	    <div className="mt-4">
	      <h1>My Cart</h1>
	      {cart.length > 0 ? (
	        <>
	          <Table striped bordered hover>
	            <thead>
	              <tr>
	                <th>Product Name</th>
	                <th>Price</th>
	                <th>Quantity</th>
	                <th>Total</th>
	                <th>Action</th>
	              </tr>
	            </thead>
	            <tbody>
	              {cart.map((item) => (
	                <tr key={item.productId}>
	                  <td>{item.productName}</td>
	                  <td>${item.price.toFixed(2)}</td>
	                  <td>{item.quantity}</td>
	                  <td>${(item.price * item.quantity
	                  	).toFixed(2)}</td>
	                  	<td>
	                  	<Button
	                  	variant="danger"
	                  	onClick={() => removeFromCart(item.productId)}
	                  	>
	                  	Remove
	                  	</Button>
	                  	</td>
	                  	</tr>
	                  	))}
	                  	</tbody>
	                  	</Table>
	                  	<div className="d-flex justify-content-end align-items-center">
	                  	<h4 className="mr-3">Total: ${getTotalPrice().toFixed(2)}</h4>
	                  	<Button variant="success" onClick={handleCheckout}>
	                  	Checkout
	                  	</Button>
	                  	</div>
	                  	</>
	                  	) : (
	                  	<p>Your cart is empty.</p>
	                  	)}
	                  	</div>
	                  	);
	                  	}

	                  	export default Cart;