import AdminDashboard from '../components/AdminDashboard';


export default function Dashboard() {
  const isAdmin = true; // replace with your actual isAdmin logic
  return (
    <>
      <AdminDashboard isAdmin={isAdmin} />
    </>
  )
}
